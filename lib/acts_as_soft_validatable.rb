module ActsAsSoftValidatable
  extend ActiveSupport::Concern

  included do
    before_update :check_soft_validator_field
  end

  cattr_accessor :enable_soft_validator

  module ClassMethods
    def acts_as_soft_validatable(options = {})
      cattr_accessor :soft_validation_target_field
      cattr_accessor :return_conditions
      self.soft_validation_target_field = options[:soft_validation_target_field] ? (options[:soft_validation_target_field]).to_s : false
      self.return_conditions = (options[:return_conditions] || {})
    end

    def soft_validates(field, options = {})
      validates(field, options_with_conditions(options))
    end

    def soft_validate(method, options = {})
      validate(method, options_with_conditions(options))
    end

    private

    def options_with_conditions(options = {})
      if_conditions = [:enable_soft_validator]
      if_conditions << options[:if] if options[:if]
      options[:if] = if_conditions
      options
    end
  end

  def soft_valid?
    enable_soft do
      valid?
    end
  end

  def soft_errors
    enable_soft do
      errors
    end
  end

  def check_soft_validator_field
    return unless soft_validation_target_field || !check_conditions
    value = valid? ? soft_valid? : false
    update_column(resource_class.soft_validation_target_field, value)
    valid?
  end

  def check_conditions
    value = false
    self.class.return_conditions.each do |condition_method, condition_value|
      value = send(condition_method.to_sym) == condition_value
      return true if value
    end
    false
  end

  private

  def resource_class
  	self.class
  end

  def enable_soft(&_block)
    return unless block_given?
    self.enable_soft_validator = true
    tmp = yield
    self.enable_soft_validator = false
    tmp
  end

end
